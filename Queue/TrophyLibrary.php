<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02/12/16
 * Time: 18:28
 */

namespace eezeecommerce\TrophyBundle\Queue;


use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\CategoryBundle\Entity\Category;
use eezeecommerce\ProductBundle\Entity\Options;
use eezeecommerce\ProductBundle\Entity\Product;
use eezeecommerce\StockBundle\Entity\Stock;
use eezeecommerce\UploadBundle\Entity\Files;
use eezeecommerce\WebBundle\Entity\Uri;
use Lsw\ApiCallerBundle\Caller\LoggingApiCaller;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Lsw\ApiCallerBundle\Call\HttpGetJson;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;

class TrophyLibrary implements ConsumerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var LoggingApiCaller
     */
    private $api;

    public function __construct(ObjectManager $om, LoggingApiCaller $api)
    {
        $this->om = $om;
        $this->api = $api;
    }

    public function execute(AMQPMessage $msg)
    {
        $nodes = unserialize($msg->getBody());

        $categories = array();
        $products = array();
        $options = array();

        if (array_key_exists("options", $nodes)) {
            if (null !== $nodes["options"]) {
                foreach ($nodes["options"] as $o) {
                    $options[] = $this->om->getRepository(Options::class)
                        ->find($o);
                }
            }
        }

        foreach ($nodes["trophies"] as $category) {
            if ($categoryChildren = $category["children"]) {
                if (count($categoryChildren) > 0) {
                    foreach($categoryChildren as $cc) {
                        if ($cc["li_attr"]["data-type"] == "trophy") {
                            $products[] = array(
                                "id" => $cc["li_attr"]["data-id"],
                                "parent" => $category["li_attr"]["data-id"],
                                "supplier" => $cc["li_attr"]["data-supplier"],
                                "supplier-category" => $cc["li_attr"]["data-supplier-category"]
                            );
                        }
                        if ($ccc = $cc["children"]) {
                            if (count($ccc) > 0) {
                                foreach ($ccc as $thirdTierChildren) {
                                    if ($thirdTierChildren["li_attr"]["data-type"] == "trophy") {
                                        $products[] = array(
                                            "id" => $thirdTierChildren["li_attr"]["data-id"],
                                            "parent" => $cc["li_attr"]["data-id"],
                                            "supplier" => $thirdTierChildren["li_attr"]["data-supplier"],
                                            "supplier-category" => $thirdTierChildren["li_attr"]["data-supplier-category"]
                                        );
                                    }
                                    if ($cccc = $thirdTierChildren["children"]) {
                                        if (count($cccc) > 0) {
                                            foreach ($cccc as $fourthTierChildren) {
                                                $products[] = array(
                                                    "id" => $fourthTierChildren["li_attr"]["data-id"],
                                                    "parent" => $thirdTierChildren["li_attr"]["data-id"],
                                                    "supplier" => $fourthTierChildren["li_attr"]["data-supplier"],
                                                    "supplier-category" => $fourthTierChildren["li_attr"]["data-supplier-category"]
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->addProduct($products, $options);

        return;

    }

    private function addProduct(array $products, array $options)
    {
        foreach ($products as $p) {
            $api = $this->api->call(
                new HttpGetJson(
                    "http://api.eezeecommerce.com/v1/supplier/".$p["supplier"]."/".$p["supplier-category"],
                    array(),
                    true
                )
            );
            foreach ($api as $result) {
                if ($result["id"] == $p["id"]) {
                    $trophy = $result;
                    break;
                }
            }
            $test = $this->om->getRepository(Product::class)->findOneBy(array("stock_code" => $trophy["code"]));
            if (count($test) < 1) {
                $category = $this->om->getRepository(Category::class)->findOneById($p["parent"]);
                if (null === $category) {
                    continue;
                }
                $uri = new Uri();
                $uri->setSlug($trophy["name"]);
                $uri->setUriKey($trophy["name"]);
                $uri->setTitle($trophy["name"]);
                $uri->setKeywords($trophy["name"]);
                $uri->setDescription($trophy["name"]);
                $this->om->persist($uri);
                $product = new Product();
                $product->setProductName($trophy["name"]);
                $product->setShortDescription($trophy["name"]);
                $product->setStockCode($trophy["code"]);
                $product->addCategory($category);
                $product->setBasePrice($trophy["price"]);
                $product->setSku($p["supplier"]."-".$trophy["code"]);
                $product->setLocale("en");
                $product->setLength(5);
                $product->setHeight(5);
                $product->setDepth(5);
                $stock = new Stock();
                $stock->setStock(0);
                $this->om->persist($stock);
                $product->setStock($stock);
                $product->setWeight(0.5);
                $product->setUri($uri);
                if (!empty($options)) {
                    foreach ($options as $option) {
                        $product->addOption($option);
                    }
                }
                if (isset($trophy["image"])) {
                    $ex = explode("/",$trophy["image"]["image_name"]);
                    $name = $ex[(count($ex) - 1)];
                    $file = new File($this->uploadImage("http://api.eezeecommerce.com".$trophy["image"]["image_name"], $name));
                    $image = new Files($file);
                    $image->setImageName($name);
                    $image->setAlt($trophy["code"]);
                    $image->setUpdatedAt(new \DateTime("now"));
                    $this->om->persist($image);
                    $product->addImage($image);
                }
                $this->om->persist($product);
            } else {
                $category = $this->om->getRepository(Category::class)->findOneById($p["parent"]);

                if (count($category) > 0) {
                    $originalCategories = new ArrayCollection();

                    foreach ($test->getCategory() as $cat) {
                        $originalCategories->add($cat);
                    }

                    if (false === $originalCategories->contains($category)) {
                        $test->addCategory($category);

                        $this->om->persist($test);
                    }
                }
            }
        }
        $this->om->flush();
    }

    private function uploadImage($path,$name) {
        $location = __DIR__."/../../../../web/files/".$name;
        $fp = fopen($location, "w+");
        $ch = curl_init($path);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        return $location;
    }
}