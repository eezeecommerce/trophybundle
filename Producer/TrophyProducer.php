<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/12/16
 * Time: 17:30
 */

namespace eezeecommerce\TrophyBundle\Producer;


use OldSound\RabbitMqBundle\RabbitMq\Producer;

class TrophyProducer
{
    private $producer;

    public function __construct(Producer $producer)
    {
        $this->producer = $producer;
    }

    public function publish(array $msg)
    {
        $this->producer->publish(serialize($msg));
    }
}