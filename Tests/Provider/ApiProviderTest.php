<?php

namespace eezeecommerce\TrophyBundle\Tests\Provider;


use eezeecommerce\TrophyBundle\Provider\ApiProvider;
use Lsw\ApiCallerBundle\Caller\LoggingApiCaller;
use Lsw\ApiCallerBundle\Logger\ApiCallLogger;

class ApiProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testApiProviderRequiresTwoParams()
    {
        $uri = "http://example.com";

        $caller = $this->getMockBuilder(ApiCallLogger::class)
            ->disableOriginalConstructor()
            ->getMock();

        new ApiProvider($caller, $uri);
    }
}