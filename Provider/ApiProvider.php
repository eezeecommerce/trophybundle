<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05/09/16
 * Time: 14:41
 */

namespace eezeecommerce\TrophyBundle\Provider;


use Lsw\ApiCallerBundle\Call\HttpGetJson;
use Lsw\ApiCallerBundle\Logger\ApiCallLogger;

class ApiProvider
{
    /**
     * @var string
     */
    private $uri;

    /**
     * @var ApiCallLogger
     */
    private $api;

    /**
     * ApiProvider constructor.
     * @param ApiCallLogger $api
     * @param $uri
     */
    public function __construct(ApiCallLogger $api, $uri)
    {
        $this->api = $api;

        $this->uri = $uri;
    }

    public function call($params)
    {
        return $this->api->calls(new HttpGetJson($this->uri, $params));
    }
}