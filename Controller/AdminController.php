<?php

namespace eezeecommerce\TrophyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AdminController extends Controller
{
    /**
     * @Route("/trophylibrary", name="eezeecommerce_trophy_index")
     */
    public function indexAction()
    {

        $options = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Options")
            ->findAll();
        return $this->render("eezeecommerceTrophyBundle:Admin:index.html.twig", array(
            "options" => $options
        ));
    }

    /**
     * @Route("/trophylibrary/discontinued", name="eezeecommerce_trophy_discontinued")
     */
    public function discontinuedAction()
    {
        return $this->render("eezeecommerceTrophyBundle:Admin:discontinued.html.twig");
    }
}
