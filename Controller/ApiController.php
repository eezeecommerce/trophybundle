<?php

namespace eezeecommerce\TrophyBundle\Controller;

use Lsw\ApiCallerBundle\Call\HttpGetJson;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * @Route("/api/trophies", name="trophy_api_trophies")
     */
    public function getProductsAction(Request $request)
    {
        $api = $this->get("api_caller")->call(new HttpGetJson("http://api.eezeecommerce.com/v1/supplier", array(), true));

        $parent = $request->query->get("parent");

        if ($parent == "#") {
            $array = array(
                    "text" => "Suppliers",
                    "id" => 0,
                    "children" => true,
                    "opened" => true,
                    "li_attr" => array(
                        "data-type" => "parent"
                    )
            );
        }else {
            foreach ($api as $item) {
                $children = array();
                if (count($item["trophy"]) > 0) {
                    if (is_int($item["id"])) {
                        $categories = $this->get("api_caller")->call(new HttpGetJson("http://api.eezeecommerce.com/v1/supplier/".$item["id"], array(), true));

                        foreach ($categories as $category) {
                            $trop = array();
                            if (count($category["trophy"]) > 0) {

                                foreach ($category["trophy"] as $trophy) {
                                    $trop[] = array(
                                        "text" => $trophy["code"]." - ".$trophy["name"],
                                        "icon" => "fa fa-trophy",
                                        "li_attr" => array(
                                            "data-type" => "trophy",
                                            "data-id" => $trophy["id"],
                                            "data-supplier" => $item["name"],
                                            "data-supplier-category" => $category["name"]
                                        )
                                    );
                                }
                            }

                            $children[] = array(
                                "text" => $category["name"],
                                "children" => $trop,
                                "li_attr" => array(
                                    "data-type" => "trophy-category",
                                    "data-id" => $category["id"],
                                    "data-supplier" => $item["id"]
                                )
                            );
                        }
                    }
                }

                $array[] = array(
                    "id" => $item["id"],
                    "text" => $item["name"],
                    "children" => $children,
                    "opened" => true,
                    "li_attr" => array(
                        "data-type" => "trophy-supplier",
                        "data-id" => $item["id"]
                    )
                );
            };
        }

        return new JsonResponse($array);
    }

    /**
     * @Route("/api/trophies/add", name="add_trophies_api")
     */
    public function addTrophiesAction(Request $request)
    {
        $trophies = $request->request->get("json");
        $options = $request->request->get("options");

        $array["trophies"] = json_decode($trophies, true);
        $array["options"] = json_decode($options, true);


        $producer = $this->get("trophy_library_producer");

        $producer->publish($array);

        return new JsonResponse($array);
    }
}