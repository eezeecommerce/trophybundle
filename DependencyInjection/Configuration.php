<?php

namespace eezeecommerce\TrophyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('eezeecommerce_trophy');

        $rootNode
            ->validate()
                ->ifTrue(function($u) {return !filter_var($u["api"]["path"], FILTER_VALIDATE_URL);})
                ->thenInvalid("eezeecommerce_trophy Api Path must be a valid url.")
            ->end()
            ->children()
                ->arrayNode("api")
                    ->children()
                        ->scalarNode("path")
                            ->isRequired(true)
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
